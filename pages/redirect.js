import Head from 'next/head'
import { useEffect } from 'react'
import { useRouter } from 'next/router'
import { Grid, Typography } from '@material-ui/core'
import axios from 'axios'

export default function Redirect () {
  const router = useRouter()

  useEffect(() => {
    const companies = ['DAISI1', 'DAISI2', 'DAISI3'];
    const company = companies[Math.floor(Math.random() * companies.length)];
    axios({
      method: 'post',
      url: 'https://api2.kadokard.com/api/v1/wa-invite',
      headers: {
        'Content-Type': 'application/json',
      },
      data: {
        message: "Selamat datang di ATI Beta Test. Silahkan kirim pesan ini untuk registrasi dan dapatkan link Lucky Draw Happy New Year Surprise",
        company: company
      }
    })
    .then(function(response) {
      router.push(response.data.url)
    })
    .catch(function(err) {
      console.log(err)
    })
  }, [])

  return (
    <div className='root'>
      <Grid container alignItems='center' justify='center' style={{ height: '100vh', width: '100vw', backgroundColor: '#eaeaea' }}>
        <Typography>Loading...</Typography>
      </Grid>
    </div>
  )
}