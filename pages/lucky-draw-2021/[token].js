import Head from 'next/head'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import { Grid, Typography } from '@material-ui/core'
import axios from 'axios'

export default function LuckyDraw () {
  const router = useRouter()
  const { token } = router.query
  const [beforeDate, setBeforeDate] = useState(false)
  const [afterDate, setAfterDate] = useState(false)
  const [rightDate, setRightDate] = useState(false)
  const [validUrl, setValidUrl] = useState(false)

  const [secretCode, setSecretCode] = useState('')

  useEffect(() => {
    // let dateNow = (new Date())
    // let newYear = (new Date("01/01/2021"))
    setRightDate(true)
    if (token) {
      axios({
        method: 'POST',
        headers: { 'content-type' : 'application/json' },
        data: { token: token },
        url: 'https://timkado-service-efair.herokuapp.com/generateCode'
      })
      .then(function (response) {
        console.log(response)
        if (response.data.success) {
          setSecretCode(response.data.code)
        } else if (!response.data.success && response.data.message === "Sudah lewat!") {
          setRightDate(false)
          setAfterDate(true)
        } else if (!response.data.success && response.data.message === "Belum mulai!") {
          setRightDate(false)
          setBeforeDate(true)
        }
        // setSecretCode(response.data.code)
      })
      .catch(function (err) {
        setRightDate(false)
        setValidUrl(true)
      })
    }
    // if (dateNow < newYear) {
    //   setRightDate(false)
    //   setAfterDate(false)
    //   setBeforeDate(true)
    // } else if (dateNow > new Date(2021, 1, 5, 9, 0, 0, 0)) {
    //   setRightDate(false)
    //   setBeforeDate(false)
    //   setAfterDate(true)
    // } else {
    //   setBeforeDate(false)
    //   setAfterDate(false)
    //   setRightDate(true)
    //   if (token) {
    //     axios({
    //       method: 'POST',
    //       headers: { 'content-type' : 'application/json' },
    //       data: { token: token },
    //       url: 'https://timkado-service-efair.herokuapp.com/generateCode'
    //     })
    //     .then(function (response) {
    //       setSecretCode(response.data.code)
    //     })
    //     .catch(function (err) {
    //       setBeforeDate(false)
    //       setAfterDate(false)
    //       setRightDate(false)
    //       setValidUrl(true)
    //     })
    //   }
      
    // }
  }, [token])

  return (
    <div className='root'>
      <Head>
        <title>Lucky Draw Secret Code</title>
        <link rel="icon" href="/favicon.ico" />
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" />
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />

        <meta property="title" content="Lucky Draw 2021" />
        <meta property="description" content="Klik link pada periode yang ditentukan untuk melihat kode anda" />
        <meta property="image" content="https://e-fair.vercel.app/thum.jpg" />

        <meta property="og:title" content="Lucky Draw 2021" />
        <meta property="og:description" content="Klik link pada periode yang ditentukan untuk melihat kode anda" />
        <meta property="og:image" content="https://e-fair.vercel.app/thum.jpg" />

      </Head>
      {
        rightDate ? 
          <Grid container className='lucky-draw-container' justify="center" alignItems="center" style={{ height: '100vh' }}>
            <Grid container item justify="center" alignItems="flex-start" className='inner-container'>
              <Grid item container xs={10} justify="center" alignItems="center">
                <img src='/happy.jpg' alt='happy new year' className='new-year-logo' />
              </Grid>
              <Grid item container xs={10} justify="center" alignItems="center">
                <Typography variant='h4' className='title' style={{ textAlign: 'center' }}>Kode rahasia anda adalah</Typography>
              </Grid>
              <Grid item container xs={10} justify="center" alignItems="center">
                <Typography variant='h3' className='code' style={{ textAlign: 'center', fontWeight: 'bold' }}>{ secretCode ? secretCode : 'Loading...' }</Typography>
              </Grid>
              <Grid item container xs={10} justify="center" alignItems="center">
                <Typography variant='subtitle1' className='description' style={{ textAlign: 'center' }}>Screenshot kode di atas sebagai verifikasi pada saat pengundian. Pemenang akan diumumkan pada <span style={{ fontWeight: 'bolder' }}>Selasa, 5 Januari 2021.</span></Typography>
              </Grid>
            </Grid>
          </Grid> : ''
      }
      {
        afterDate ?
          <Grid container className='lucky-draw-container' justify="center" alignItems="center" style={{ height: '100vh' }}>
            <Grid container item justify="center" alignItems="flex-start" className='inner-container'>
              <Grid item container xs={10} justify="center" alignItems="center">
                <img src='/happy.jpg' alt='happy new year' className='new-year-logo' />
              </Grid>
              <Grid item container xs={10} justify="center" alignItems="center">
                <Typography variant='h4' className='title' style={{ textAlign: 'center', marginBottom: '12vh' }}>Maaf link lucky draw sudah tidak aktif setelah tanggal 5 Januari 2021 pukul 09.00 WIB.</Typography>
              </Grid>
            </Grid>
          </Grid> : ''
      }
      {
        beforeDate ?
          <Grid container className='lucky-draw-container' justify="center" alignItems="center" style={{ height: '100vh' }}>
            <Grid container item justify="center" alignItems="flex-start" className='inner-container'>
              <Grid item container xs={10} justify="center" alignItems="center">
                <img src='/happy.jpg' alt='happy new year' className='new-year-logo' />
              </Grid>
              <Grid item container xs={10} justify="center" alignItems="center">
                <Typography variant='h4' className='title' style={{ textAlign: 'center', marginBottom: '12vh' }}>Anda akan mendapatkan kode lucky draw pada tanggal 1 Januari 2021, mohon klik kembali link ini pada periode tanggal 1 Januari 2021 - 5 Januari 2021 pukul 09.00 WIB.</Typography>
              </Grid>
            </Grid>
          </Grid> : ''
      }
      {
        validUrl ?
          <Grid container className='lucky-draw-container' justify="center" alignItems="center" style={{ height: '100vh' }}>
            <Grid container item justify="center" alignItems="flex-start" className='inner-container'>
              <Grid item container xs={10} justify="center" alignItems="center">
                <img src='/happy.jpg' alt='happy new year' className='new-year-logo' />
              </Grid>
              <Grid item container xs={10} justify="center" alignItems="center">
                <Typography variant='h4' className='title' style={{ textAlign: 'center', marginBottom: '12vh' }}>URL yang anda gunakan tidak valid, mohon gunakan URL yang telah diberikan oleh CS kami.</Typography>
              </Grid>
            </Grid>
          </Grid> : ''
      }
    </div>
  )
}