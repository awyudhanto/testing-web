import 'date-fns';
import Head from 'next/head'
import { useState } from 'react'
import { Grid, Typography, Hidden, Link, Button, TextField, FormControl, InputLabel, Select, MenuItem, Checkbox, Snackbar } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles';
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';
import AccountCircle from '@material-ui/icons/AccountCircle';
import FaceIcon from '@material-ui/icons/Face';
import TodayIcon from '@material-ui/icons/Today';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import SendIcon from '@material-ui/icons/Send';

const useStyles = makeStyles((theme) => ({
  form: {
    width: '100%',
    alignItems: 'flex-end'
  },
  inputElement: {
    padding: theme.spacing(1),
    width: '100%'
  },
  formControl: {
    width: '90%',
  },
  formInside: {
    width: '100%'
  },
  iconControl: {
    width: '10%',
  },
  locationFormControl: {
    width: '45%'
  },
  inputForm: {
    width: '100%',
    fontWeight: 700,
  },
  checkboxContainer: {
    paddingTop: theme.spacing(3),
  },
  checkboxCaption: {
    height: '100%',
    alignSelf: 'center'
  }
}));

export default function Registration() {
  const classes = useStyles();
  const [biodata, setBiodata] = useState({
    fullName: '',
    gender: '',
    birthdate: null,
    location: '',
    locationDetails: ''
  })

  const [checked, setChecked] = useState(false)
  const [toast, setToast] = useState({
    open: false,
    vertical: 'bottom',
    horizontal: 'right',
    message: ''
  });

  const { vertical, horizontal, open, message } = toast;

  const jakartaLocations = ['Jakarta Pusat', 'Jakarta Barat', 'Jakarta Utara', 'Jakarta Selatan', 'Jakarta Timur']
  const tangerangLocations = ['Batuceper', 'Benda', 'Cibodas', 'Ciledug', 'Cipondoh', 'Jatiuwung', 'Karangtengah', 'Karawaci', 'Larangan', 'Neglasari', 'Periuk', 'Pinang', 'Tangerang']

  function handleOnChange (event) {
    const newBiodata = JSON.parse(JSON.stringify(biodata))
    if (event && event.target) {
      newBiodata[event.target.name] = event.target.value
    } else {
      newBiodata.birthdate = event
    }
    setBiodata(newBiodata)
  }

  const handleCheck = (event) => {
    setChecked(event.target.checked);
  };

  const openToast = (newState) => {
    console.log('masuk')
    setToast({ open: true, ...newState });
  };

  const closeToast = () => {
    setToast({ ...toast, open: false, message: '' });
  };

  function handleOnSubmit () {
    if (!biodata.fullName || !biodata.gender || !biodata.birthdate || !biodata.location || !biodata.locationDetails) {
      openToast({ message: 'you must fill all available forms', vertical: 'bottom', horizontal: 'right' })
    } else if (!checked) {
      openToast({ message: 'you must agree to the Terms and Conditions', vertical: 'bottom', horizontal: 'right' })
    } else {

    }
  }

  return (
    <div className='root'>
      <Head>
        <title>E-Fair Registration</title>
        <link rel="icon" href="/favicon.ico" />
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" />
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
      </Head>

      <Grid container className='first-container' justify="center" alignItems="center" style={{ height: '100vh' }}>
        <Grid item container xs={12} justify='center'>
          <Hidden mdDown>
            <Grid item xs={2} sm={2} md={1} />
          </Hidden>
          <Grid item xs={1} />
          <Grid item container xs={10} sm={6} md={4} justify='center' alignItems="center">
            <Hidden mdUp>
              <Grid item container xs={10} justify='center' alignItems="center">
                <img src='/efair-logo.png' alt="e-fair" className='efair-logo-mobile' />
              </Grid>
            </Hidden>
            <Grid item container xs={10} justify='center' alignItems="center">
              <img src='/registration.png' alt="registration form" className='regis-logo' />
            </Grid>
            <form autoComplete="off" className={classes.form}>
              <div className={classes.inputElement}>
                <Grid container spacing={1} alignItems="flex-end">
                  <Grid item className={classes.iconControl}>
                    <AccountCircle />
                  </Grid>
                  <Grid item className={classes.formControl}>
                    <TextField
                      name='fullName'
                      label="Full Name"
                      className={classes.inputForm}
                      value={biodata.fullName}
                      onChange={(event) => handleOnChange(event)}
                      InputProps={{
                        classes: {
                          input: classes.inputForm
                        }
                      }}
                    />
                  </Grid>
                </Grid>
              </div>

              <div className={classes.inputElement}>
                <Grid container spacing={1} alignItems="flex-end">
                  <Grid item className={classes.iconControl}>
                    <FaceIcon />
                  </Grid>
                  <Grid item className={classes.formControl}>
                    <FormControl className={classes.formInside}>
                      <InputLabel>Gender</InputLabel>
                      <Select
                        name='gender'
                        className={classes.inputForm}
                        value={biodata.gender}
                        onChange={(event) => handleOnChange(event)}
                      >
                        <MenuItem value={'male'}>Male</MenuItem>
                        <MenuItem value={'female'}>Female</MenuItem>
                      </Select>
                    </FormControl>
                  </Grid>
                </Grid>
              </div>

              <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <div className={classes.inputElement}>
                  <Grid container spacing={1} alignItems="flex-end">
                    <Grid item className={classes.iconControl}>
                      <TodayIcon />
                    </Grid>
                    <Grid item className={classes.formControl}>
                      <KeyboardDatePicker
                        disableToolbar
                        className={classes.inputForm}
                        value={biodata.birthdate}
                        onChange={(event) => handleOnChange(event)}
                        variant="inline"
                        format="MM/dd/yyyy"
                        id="date-picker-inline"
                        label="Birthdate (mm/dd/yyyy)"
                        name='birthdate'
                        InputProps={{
                          classes: {
                            input: classes.inputForm
                          }
                        }}
                        KeyboardButtonProps={{
                          'aria-label': 'change date',
                        }}
                      />
                    </Grid>
                  </Grid>
                </div>
              </MuiPickersUtilsProvider>

              <div className={classes.inputElement}>
                <Grid container spacing={1} alignItems="flex-end">
                  <Grid item className={classes.iconControl}>
                    <LocationOnIcon />
                  </Grid>
                  <Grid item className={classes.formControl}>
                    <FormControl className={classes.locationFormControl}>
                      <InputLabel>Location</InputLabel>
                      <Select
                        name='location'
                        className={classes.inputForm}
                        value={biodata.location}
                        onChange={(event) => handleOnChange(event)}
                      >
                        <MenuItem value={'jakarta'}>Jakarta</MenuItem>
                        <MenuItem value={'tangerang'}>Tangerang</MenuItem>
                      </Select>
                    </FormControl>
                    {
                      biodata.location === '' ? '' : (
                        <FormControl className={classes.locationFormControl} style={{ marginLeft: '10%' }}>
                          <InputLabel>Details</InputLabel>
                          <Select
                            name='locationDetails'
                            className={classes.inputForm}
                            value={biodata.locationDetails}
                            onChange={(event) => handleOnChange(event)}
                          >
                            {
                              biodata.location === 'jakarta' ?
                                jakartaLocations.map((location, index) => {
                                  return <MenuItem value={ location } key={ index }>{ location }</MenuItem>
                                }) :
                                tangerangLocations.map((location, index) => {
                                  return <MenuItem value={ location } key={ index }>{ location }</MenuItem>
                                })
                            }
                          </Select>
                        </FormControl>
                      )
                    }
                  </Grid>
                </Grid>
              </div>

              <div className={classes.checkboxContainer}>
                <Grid container spacing={1} alignItems="flex-end">
                  <Grid item className={classes.checkbox}>
                    <Checkbox
                      checked={checked}
                      onChange={handleCheck}
                      inputProps={{ 'aria-label': 'primary checkbox' }}
                    />
                  </Grid>
                  <Grid item className={classes.checkboxCaption}>
                    <Typography variant='subtitle2'>I agree to the <Link href='https://ui.daisi.id/landing/timkado/privacypolicy.html' target='_blank' style={{ fontWeight: 'bold', fontStyle: 'italic' }}>Terms and Conditions</Link></Typography>
                  </Grid>
                </Grid>
              </div>

              <Button
                variant="contained"
                color="primary"
                className={classes.inputElement}
                onClick={() => handleOnSubmit()}
                style={{ marginTop: '3%' }}
                endIcon={<SendIcon />}
              >
                Send
              </Button>
            </form>
          </Grid>
          <Grid item xs={1} />
          <Hidden smDown>
            <Grid item container xs={4} alignItems="center">
              <Grid item container xs={10} justify='center' alignItems="center">
                <img src='/efair-logo.png' alt="e-fair" className='efair-logo' />
              </Grid>
            </Grid>
          </Hidden>
          <Hidden mdDown>
            <Grid item xs={2} sm={2} md={1} />
          </Hidden>
        </Grid>
      </Grid>

      <Snackbar 
        anchorOrigin={{ vertical, horizontal }}
        open={open}
        autoHideDuration={5000}
        onClose={closeToast}
        message={message}
        key={vertical + horizontal}
      />
    </div>
  )
}
