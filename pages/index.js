import Head from 'next/head'
import { Grid, Typography, Hidden, Breadcrumbs, Link, Button } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles';
import LiveHelpIcon from '@material-ui/icons/LiveHelp';
import PermPhoneMsgIcon from '@material-ui/icons/PermPhoneMsg';
import PeopleIcon from '@material-ui/icons/People';
import SendIcon from '@material-ui/icons/Send';

const useStyles = makeStyles((theme) => ({
  text: {
    padding: theme.spacing(2),
    textAlign: 'center',    
    fontWeight: 'bolder'
  },
  svgLarge: {
    width: theme.spacing(25),
    height: theme.spacing(25),
  },
  link: {
    display: 'flex',
  },
  icon: {
    marginRight: theme.spacing(1),
    width: 20,
    height: 20,
  },
  nav: {
    position: 'absolute',
    top: '15px',
    right: '15px'
  },
  ticketButton: {
    margin: theme.spacing(3),
  }
}));

export default function Home() {
  const classes = useStyles();

  const sponsors = [
    {
      name: 'garuda-indonesia',
      path: 'sponsors/Garuda_Indonesia_logo.svg'
    },
    {
      name: 'mitsubishi-motors',
      path: 'sponsors/Mitsubishi_motors_logo.svg'
    },
    {
      name: 'lion-air',
      path: 'sponsors/Lion_Air_logo.svg'
    },
    {
      name: 'shell',
      path: 'sponsors/Shell_logo.svg'
    },
    {
      name: 'gilead-science',
      path: 'sponsors/Gilead_Sciences_logo.svg'
    },
    {
      name: 'total',
      path: 'sponsors/Total_logo.svg'
    }
  ]

  return (
    <div className='root'>
      <Head>
        <title>E-Fair</title>
        <link rel="icon" href="/favicon.ico" />
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" />
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
      </Head>

      <Grid container className='first-container' justify="center" alignItems="center" style={{ height: '100vh' }}>
        <Breadcrumbs aria-label="breadcrumb" className={classes.nav}>
          <Link color="inherit" href="#about-us" className={classes.link}>
            <PeopleIcon className={classes.icon} />
            About Us
          </Link>
          <Link
            color="inherit"
            href="https://wa.me/6281219523558?text=Hi Timkado's CS"
            target="_blank"
            className={classes.link}
          >
            <PermPhoneMsgIcon className={classes.icon} />
            Contact Us
          </Link>
          <Link
            color="inherit"
            href="#"
            className={classes.link}
          >
            <LiveHelpIcon className={classes.icon} />
            FAQ
          </Link>
        </Breadcrumbs>
        <Grid item container xs={12} justify='center'>
          <Grid item container xs={10} justify='center' alignItems="center">
            <img src='/efair-logo.png' alt="e-fair" className='efair-logo' />
          </Grid>
          <Grid item xs={10}>
            <Typography variant='subtitle1' className={classes.text} style={{ color: 'black' }}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque pharetra nisl quis sapien finibus sagittis. Aenean vulputate libero eget ex congue, non laoreet sem aliquet. Donec volutpat, nisl et finibus pharetra, justo ligula faucibus ligula, nec sagittis mauris ex sed leo.</Typography>
          </Grid>
          <Grid item container xs={10} justify='center' alignItems="center">
            <Button variant="contained" color="primary" className={classes.ticketButton} endIcon={<SendIcon />}>Get Your Ticket Now</Button>
          </Grid>
        </Grid>
      </Grid>
      <Grid container id='about-us' className='second-container' justify="center" alignItems="center">
        <Grid item container xs={12} justify='center'>
          <Grid item xs={10}>
            <Typography variant='h2' className={classes.text} style={{ color: '#eaeaea' }}>About Us</Typography>
          </Grid>
          <Grid item xs={10}>
            <Typography variant='subtitle1' className={classes.text} style={{ color: '#eaeaea' }}>The DAISI platform leverages popular messaging applications such as Whatsapp to provide the most engaging marketing promotions or virtual event</Typography>
          </Grid>
        </Grid>
        <Grid item container spacing={6} xs={12} justify='center' style={{ marginTop: '25px' }}>
          {/* <Grid item xs={2} sm={2} md={1} />
          <Grid item container xs={7} sm={4} md={2}>
            <Grid item xs={12}>
              <img src='/plane-white.svg' alt="Virtual Airline Ticket Booths" className='about-us-logo' />
            </Grid>
            <Grid item xs={12}>
              <Typography variant='subtitle1' className={classes.text} style={{ color: '#eaeaea' }}>Virtual Airline Ticket Booths</Typography>
            </Grid>
          </Grid>
          <Hidden smUp>
            <Grid item xs={2} sm={2} />
          </Hidden>
          <Hidden smUp>
            <Grid item xs={2} sm={2} />
          </Hidden>
          <Grid item xs={7} sm={4} md={2}>
          <Grid item xs={12}>
              <img src='/hotel-white.svg' alt="Virtual Hotel Booths" className='about-us-logo' />
            </Grid>
            <Grid item xs={12}>
              <Typography variant='subtitle1' className={classes.text} style={{ color: '#eaeaea' }}>Virtual Hotel Booths</Typography>
            </Grid>
          </Grid>
          <Hidden mdUp>
            <Grid item xs={2} sm={2} />
          </Hidden>
          <Grid item xs={7} sm={4} md={2}>
            <Grid item xs={12}>
              <img src='/mountains-white.svg' alt="Virtual Tour Package Booths" className='about-us-logo' />
            </Grid>
            <Grid item xs={12}>
              <Typography variant='subtitle1' className={classes.text} style={{ color: '#eaeaea' }}>Virtual Tour Package Booths</Typography>
            </Grid>
          </Grid> */}
          <Grid item xs={7} sm={4} md={2}>
            <Grid item xs={12}>
              <img onClick={()=> window.open("https://wa.me/6281219523558?text=Hi Timkado's CS", "_blank")} src='/CS-white.svg' alt="Talk To A Customer Service" className='about-us-logo' />
            </Grid>
            <Grid item xs={12}>
              <Typography variant='subtitle1' className={classes.text} style={{ color: '#eaeaea' }}>Talk To Our Customer Service</Typography>
            </Grid>
          </Grid>
          {/* <Hidden smDown>
            <Grid item xs={2} sm={2} md={1} />
          </Hidden> */}
        </Grid>
      </Grid>
      <Grid container className='third-container' justify="center" alignItems="center">
        <Grid item container xs={12} justify='center'>
          <Grid item xs={10}>
            <Typography variant='h2' className={classes.text} style={{ color: '#272727' }}>Our Sponsors</Typography>
          </Grid>
          <Grid item container xs={10} spacing={3} justify='center' alignItems="center">
            {
              sponsors.map((sponsor, index) => {
                return (
                  <Grid item xs={6} ms={4} md={2} key={index}>
                    <img src={sponsor.path} alt={sponsor.name} className='sponsor-logo' />
                  </Grid>
                )
              })
            }
          </Grid>
        </Grid>        
      </Grid>
    </div>
  )
}
