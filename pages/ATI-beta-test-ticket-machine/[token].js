import 'date-fns';
import Head from 'next/head'
import { useState } from 'react'
import { Grid, Typography, Hidden, Link, Button, TextField, Snackbar, FormControl, InputLabel, Select, MenuItem, FormLabel, Radio, RadioGroup, FormControlLabel } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles';
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';
import AccountCircle from '@material-ui/icons/AccountCircle';
import FaceIcon from '@material-ui/icons/Face';
import EmailIcon from '@material-ui/icons/Email';
import TodayIcon from '@material-ui/icons/Today';
import SendIcon from '@material-ui/icons/Send';
import { useRouter } from 'next/router'
import axios from 'axios'

const useStyles = makeStyles((theme) => ({
  form: {
    width: '100%',
    alignItems: 'flex-end'
  },
  inputElement: {
    padding: theme.spacing(1),
    width: '100%'
  },
  formControl: {
    width: '90%',
  },
  formInside: {
    width: '100%'
  },
  iconControl: {
    width: '10%',
  },
  locationFormControl: {
    width: '45%'
  },
  inputForm: {
    width: '100%',
    fontWeight: 700,
  },
  checkboxContainer: {
    paddingTop: theme.spacing(3),
  },
  checkboxCaption: {
    height: '100%',
    alignSelf: 'center'
  }
}));

export default function ATIBetaTestRegistration() {
  const classes = useStyles();
  const router = useRouter()
  const { token } = router.query
  const [biodata, setBiodata] = useState({
    name: '',
    email: '',
    sex: '',
    birthday: '',
  })

  const [toast, setToast] = useState({
    open: false,
    vertical: 'bottom',
    horizontal: 'right',
    message: ''
  });

  const { vertical, horizontal, open, message } = toast;

  function handleOnChange (event) {
    const newBiodata = JSON.parse(JSON.stringify(biodata))
    newBiodata[event.target.name] = event.target.value
    // if (event && event.target) {
    //   newBiodata[event.target.name] = event.target.value
    // } else {
    //   newBiodata.birthday = event
    // }
    setBiodata(newBiodata)
    console.log(newBiodata)
  }

  const openToast = (newState) => {
    setToast({ open: true, ...newState });
  };

  const closeToast = () => {
    setToast({ ...toast, open: false, message: '' });
  };

  function handleOnSubmit () {
    if (!biodata.name || !biodata.sex || !biodata.email || !biodata.birthday) {
      openToast({ message: 'anda harus mengisi semua form yang tersedia', vertical: 'bottom', horizontal: 'right' })
    } else {
      axios({
        method: 'POST',
        headers: { 'content-type' : 'application/json' },
        data: { ...biodata },
        url: "https://api2.kadokard.com/api/v1/f/ticket1/" + token
      })
      .then(function (response) {
        console.log(response)
        if (response.data.ok === false) throw ({ error: 'not found PID' })
        openToast({ message: 'selamat anda telah berhasil mendaftar, sekarang anda bisa menutup halaman ini.', vertical: 'bottom', horizontal: 'right' })
      })
      .catch(function (error) {
        openToast({ message: 'profile anda tidak ditemukan, silakan menggunakan link yang valid', vertical: 'bottom', horizontal: 'right' })
      });
    }
  }

  return (
    <div className='root'>
      <Head>
        <title>ATI Beta Test Ticket Machine</title>
        <link rel="icon" href="/favicon.ico" />
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" />
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />

        <meta property="title" content="Ticket Machine Registration 2021" />
        <meta property="description" content="Bagikan link ini ke member lain untuk mengikuti Lucky Draw" />
        <meta property="image" content="https://e-fair.vercel.app/rect24.png" />

        <meta property="og:title" content="Ticket Machine Registration 2021" />
        <meta property="og:description" content="Bagikan link ini ke member lain untuk mengikuti Lucky Draw" />
        <meta property="og:image" content="https://e-fair.vercel.app/rect24.png" />

      </Head>

      <Grid container className='first-container' justify="center" alignItems="center" style={{ height: '100vh' }}>
        <Grid item container xs={12} justify='center'>
          <Hidden mdDown>
            <Grid item xs={2} sm={2} md={1} />
          </Hidden>
          <Grid item xs={1} />
          <Grid item container xs={10} sm={6} md={4} justify='center' alignItems="center">
            <Hidden mdUp>
              <Grid item container xs={10} justify='center' alignItems="center">
                <img src='/beta-test-ticket-machine.png' alt="e-fair" className='efair-logo-mobile' />
              </Grid>
            </Hidden>
            <Grid item container xs={7} sm={10} justify='center' alignItems="center">
              <img src='/registration.png' alt="registration form" className='regis-logo' />
            </Grid>
            <form autoComplete="off" className={classes.form}>
              <div className={classes.inputElement}>
                <Grid container spacing={1} alignItems="flex-end">
                  <Grid item className={classes.iconControl}>
                    <AccountCircle />
                  </Grid>
                  <Grid item className={classes.formControl}>
                    <TextField
                      name='name'
                      label="Nama Lengkap"
                      className={classes.inputForm}
                      value={biodata.name}
                      onChange={(event) => handleOnChange(event)}
                      InputProps={{
                        classes: {
                          input: classes.inputForm
                        }
                      }}
                    />
                  </Grid>
                </Grid>
              </div>

              {/* <div className={classes.inputElement}>
                <Grid container spacing={1} alignItems="flex-end">
                  <Grid item className={classes.iconControl}>
                    <FaceIcon />
                  </Grid>
                  <Grid item className={classes.formControl}>
                    <FormControl className={classes.formInside}>
                      <InputLabel>Jenis Kelamin</InputLabel>
                      <Select
                        name='sex'
                        className={classes.inputForm}
                        value={biodata.sex}
                        onChange={(event) => handleOnChange(event)}
                      >
                        <MenuItem value={'male'}>Laki-laki</MenuItem>
                        <MenuItem value={'female'}>Perempuan</MenuItem>
                      </Select>
                    </FormControl>
                  </Grid>
                </Grid>
              </div> */}

              <div className={classes.inputElement} style={{ paddingTop: 30 }}>
                <Grid container spacing={1} alignItems="flex-end">
                  <Grid item container alignItems='flex-start' className={classes.iconControl} style={{ alignSelf: 'flex-start' }}>
                    <FaceIcon />
                  </Grid>
                  <Grid item className={classes.formControl}>
                    <FormControl className={classes.formInside}>
                      <FormLabel component="legend">Jenis Kelamin</FormLabel>
                      <RadioGroup className={classes.inputForm} row aria-label="position" name="sex" onChange={(event) => handleOnChange(event)} value={biodata.sex}>
                        <FormControlLabel value="male" control={<Radio color="primary" />} label="Laki-laki" />
                        <FormControlLabel value="female" control={<Radio color="primary" />} label="Perempuan" />
                      </RadioGroup>
                    </FormControl>
                  </Grid>
                </Grid>
              </div>

              <div className={classes.inputElement}>
                <Grid container spacing={1} alignItems="flex-end">
                  <Grid item className={classes.iconControl}>
                    <EmailIcon />
                  </Grid>
                  <Grid item className={classes.formControl}>
                    <TextField
                      name='email'
                      label="Email"
                      className={classes.inputForm}
                      value={biodata.email}
                      onChange={(event) => handleOnChange(event)}
                      InputProps={{
                        classes: {
                          input: classes.inputForm
                        }
                      }}
                    />
                  </Grid>
                </Grid>
              </div>

              <div className={classes.inputElement}>
                <Grid container spacing={1} alignItems="flex-end">
                  <Grid item className={classes.iconControl}>
                    <TodayIcon />
                  </Grid>
                  <Grid item className={classes.formControl}>
                    <TextField
                      name='birthday'
                      label="Tanggal Lahir (format dd-MM-yyyy)"
                      className={classes.inputForm}
                      value={biodata.birthday}
                      onChange={(event) => handleOnChange(event)}
                      InputProps={{
                        classes: {
                          input: classes.inputForm
                        }
                      }}
                    />
                  </Grid>
                </Grid>
              </div>

              <Button
                variant="contained"
                color="primary"
                className={classes.inputElement}
                onClick={() => handleOnSubmit()}
                style={{ marginTop: '7%' }}
                endIcon={<SendIcon />}
              >
                Daftar Auto Fair
              </Button>

              <Typography variant='subtitle2' style={{ textAlign: 'center', marginTop: '5%' }}>Dengan mendaftar, anda telah menyetujui <Link href='https://ui.daisi.id/landing/timkado/privacypolicy.html' target='_blank' style={{ fontWeight: 'bold', fontStyle: 'italic' }}>Syarat dan Ketentuan.</Link></Typography>
            </form>
          </Grid>
          <Grid item xs={1} />
          <Hidden smDown>
            <Grid item container xs={4} alignItems="center">
              <Grid item container xs={10} justify='center' alignItems="center">
                <img src='/beta-test-ticket-machine.png' alt="e-fair" className='beta-test-logo' />
              </Grid>
            </Grid>
          </Hidden>
          <Hidden mdDown>
            <Grid item xs={2} sm={2} md={1} />
          </Hidden>
        </Grid>
      </Grid>

      <Snackbar 
        anchorOrigin={{ vertical, horizontal }}
        open={open}
        autoHideDuration={5000}
        onClose={closeToast}
        message={message}
        key={vertical + horizontal}
      />
    </div>
  )
}
