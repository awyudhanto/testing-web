import 'date-fns';
import Head from 'next/head'
import { useRouter } from 'next/router'
import { useState } from 'react'
import { Grid, Hidden, Button, TextField, Snackbar, FormControl, FormLabel, Radio, InputLabel, Select, MenuItem, Switch, FormControlLabel, RadioGroup } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles';
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';
import AccountCircle from '@material-ui/icons/AccountCircle';
import FaceIcon from '@material-ui/icons/Face';
import EmailIcon from '@material-ui/icons/Email';
import TodayIcon from '@material-ui/icons/Today';
import SendIcon from '@material-ui/icons/Send';
import axios from 'axios'

const useStyles = makeStyles((theme) => ({
  form: {
    width: '100%',
    alignItems: 'flex-end'
  },
  inputElement: {
    padding: theme.spacing(1),
    width: '100%'
  },
  formControl: {
    width: '90%',
  },
  formInside: {
    width: '100%'
  },
  iconControl: {
    width: '10%',
  },
  locationFormControl: {
    width: '45%'
  },
  inputForm: {
    width: '100%',
    fontWeight: 700,
  },
  checkboxContainer: {
    paddingTop: theme.spacing(3),
  },
  checkboxCaption: {
    height: '100%',
    alignSelf: 'center'
  }
}));

export default function LuckyDrawRegistration() {
  const router = useRouter()
  const classes = useStyles();
  const { pid } = router.query

  const [biodata, setBiodata] = useState({
    firstName: '',
    lastName: '',
    email: '',
    gender: '',
    birthdate: '',
  })

  const [checked, setChecked] = useState('')

  const [toast, setToast] = useState({
    open: false,
    vertical: 'bottom',
    horizontal: 'right',
    message: ''
  });

  const { vertical, horizontal, open, message } = toast;

  function handleOnChange (event) {
    console.log(event)
    const newBiodata = JSON.parse(JSON.stringify(biodata))
    newBiodata[event.target.name] = event.target.value
    // if (event && event.target) {
    //   newBiodata[event.target.name] = event.target.value
    // } else {
    //   newBiodata.birthdate = event
    // }
    setBiodata(newBiodata)
  }

  function handleChecked (event) {
    setChecked(event.target.checked)
  }

  const openToast = (newState) => {
    setToast({ open: true, ...newState });
  };

  const closeToast = () => {
    setToast({ ...toast, open: false, message: '' });
  };

  function handleOnSubmit () {
    if (!biodata.firstName || !biodata.gender || !biodata.lastName || !biodata.email || !biodata.birthdate) {
      openToast({ message: 'anda harus mengisi seluruh form terlebih dahulu sebelum mendaftar', vertical: 'bottom', horizontal: 'right' })
    } else if (!pid) {
      openToast({ message: 'invalid pid', vertical: 'bottom', horizontal: 'right' })
    } else {
      axios({
        method: 'get',
        url: 'https://api2.kadokard.com/api/v1/p/' + pid,
        headers: {
          'Content-Type': 'application/json',
        },
      })
      .then(function (response) {
        if (!response.data.data) {
          throw ( response.data.msg )
        } else {
          axios({
            method: 'POST',
            headers: { 'content-type' : 'application/json' },
            data: {...biodata, phone: response.data.data.phone},
            url: 'https://timkado-service-efair.herokuapp.com/register'
          })
        }
      })  
      .then(function (response) {
        openToast({ message: 'selamat anda telah berhasil mendaftar, sekarang anda bisa menutup halaman ini.', vertical: 'bottom', horizontal: 'right' })
      })
      .catch(function (error) {
        openToast({ message: 'profile anda tidak ditemukan, silakan menggunakan link yang valid', vertical: 'bottom', horizontal: 'right' })
      });
    }
  }

  return (
    <div className='root'>
      <Head>
        <title>Lucky Draw Registration</title>
        <link rel="icon" href="/favicon.ico" />
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" />
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />

        <meta property="title" content="Lucky Draw Registration 2021" />
        <meta property="description" content="Daftarkan diri anda untuk mendapatkan tiket Lucky Draw" />
        <meta property="image" content="https://e-fair.vercel.app/lucky-draw-reg.png" />

        <meta property="og:title" content="Lucky Draw Registration 2021" />
        <meta property="og:description" content="Daftarkan diri anda untuk mendapatkan tiket Lucky Draw" />
        <meta property="og:image" content="https://e-fair.vercel.app/lucky-draw-reg.png" />

      </Head>

      <Grid container className='first-container' justify="center" alignItems="center" style={{ height: '100vh' }}>
        <Grid item container xs={12} justify='center'>
          <Hidden mdDown>
            <Grid item xs={2} sm={2} md={1} />
          </Hidden>
          <Grid item xs={1} />
          <Grid item container xs={10} sm={6} md={4} justify='center' alignItems="center">
            <Hidden mdUp>
              <Grid item container xs={10} justify='center' alignItems="center">
                <img src='/luckyy-draw-register.png' alt="e-fair" className='efair-logo-mobile' />
              </Grid>
            </Hidden>
            <form autoComplete="off" className={classes.form}>
              <div className={classes.inputElement}>
                <Grid container spacing={1} alignItems="flex-end">
                  <Grid item className={classes.iconControl}>
                    <AccountCircle />
                  </Grid>
                  <Grid item className={classes.formControl}>
                    <TextField
                      name='firstName'
                      label="Nama Depan"
                      className={classes.inputForm}
                      value={biodata.firstName}
                      onChange={(event) => handleOnChange(event)}
                      InputProps={{
                        classes: {
                          input: classes.inputForm
                        }
                      }}
                    />
                  </Grid>
                </Grid>
              </div>

              <div className={classes.inputElement}>
                <Grid container spacing={1} alignItems="flex-end">
                  <Grid item className={classes.iconControl}>
                    <AccountCircle />
                  </Grid>
                  <Grid item className={classes.formControl}>
                    <TextField
                      name='lastName'
                      label="Nama Belakang"
                      className={classes.inputForm}
                      value={biodata.lastName}
                      onChange={(event) => handleOnChange(event)}
                      InputProps={{
                        classes: {
                          input: classes.inputForm
                        }
                      }}
                    />
                  </Grid>
                </Grid>
              </div>

              <div className={classes.inputElement} style={{ paddingTop: 30 }}>
                <Grid container spacing={1} alignItems="flex-end">
                  <Grid item container alignItems='flex-start' className={classes.iconControl} style={{ alignSelf: 'flex-start' }}>
                    <FaceIcon />
                  </Grid>
                  <Grid item className={classes.formControl}>
                    <FormControl className={classes.formInside}>
                      <FormLabel component="legend">Jenis Kelamin</FormLabel>
                      <RadioGroup className={classes.inputForm} row aria-label="position" name="gender" onChange={(event) => handleOnChange(event)} value={biodata.gender}>
                        <FormControlLabel value="male" control={<Radio color="primary" />} label="Laki-laki" />
                        <FormControlLabel value="female" control={<Radio color="primary" />} label="Perempuan" />
                      </RadioGroup>
                    </FormControl>
                  </Grid>
                </Grid>
              </div>

              {/* <div className={classes.inputElement}>
                <Grid container spacing={1} alignItems="flex-end">
                  <Grid item className={classes.iconControl}>
                    <FaceIcon />
                  </Grid>
                  <Grid item className={classes.formControl}>
                    <FormControl className={classes.formInside}>
                      <InputLabel>Jenis Kelamin</InputLabel>
                      <Select
                        name='gender'
                        className={classes.inputForm}
                        value={biodata.gender}
                        onChange={(event) => handleOnChange(event)}
                      >
                        <MenuItem value={'male'}>Laki-laki</MenuItem>
                        <MenuItem value={'female'}>Perempuan</MenuItem>
                      </Select>
                    </FormControl>
                  </Grid>
                </Grid>
              </div> */}

              <div className={classes.inputElement}>
                <Grid container spacing={1} alignItems="flex-end">
                  <Grid item className={classes.iconControl}>
                    <EmailIcon />
                  </Grid>
                  <Grid item className={classes.formControl}>
                    <TextField
                      name='email'
                      label="Email"
                      className={classes.inputForm}
                      value={biodata.email}
                      onChange={(event) => handleOnChange(event)}
                      InputProps={{
                        classes: {
                          input: classes.inputForm
                        }
                      }}
                    />
                  </Grid>
                </Grid>
              </div>

              <div className={classes.inputElement}>
                <Grid container spacing={1} alignItems="flex-end">
                  <Grid item className={classes.iconControl}>
                    <TodayIcon />
                  </Grid>
                  <Grid item className={classes.formControl}>
                    <TextField
                      name='birthdate'
                      label="Tanggal Lahir (format dd-MM-yyyy)"
                      className={classes.inputForm}
                      value={biodata.birthdate}
                      onChange={(event) => handleOnChange(event)}
                      InputProps={{
                        classes: {
                          input: classes.inputForm
                        }
                      }}
                    />
                  </Grid>
                </Grid>
              </div>

              <Button
                variant="contained"
                color="primary"
                className={classes.inputElement}
                onClick={() => handleOnSubmit()}
                style={{ marginTop: '7%' }}
                endIcon={<SendIcon />}
              >
                Daftar
              </Button>
            </form>
          </Grid>
          <Grid item xs={1} />
          <Hidden smDown>
            <Grid item container xs={4} alignItems="center">
              <Grid item container xs={10} justify='center' alignItems="center">
                <img src='/luckyy-draw-register.png' alt="e-fair" className='beta-test-logo' />
              </Grid>
            </Grid>
          </Hidden>
          <Hidden mdDown>
            <Grid item xs={2} sm={2} md={1} />
          </Hidden>
        </Grid>
      </Grid>

      <Snackbar 
        anchorOrigin={{ vertical, horizontal }}
        open={open}
        autoHideDuration={5000}
        onClose={closeToast}
        message={message}
        key={vertical + horizontal}
      />
    </div>
  )
}
